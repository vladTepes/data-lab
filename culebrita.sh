#!/bin/bash

#Script para resumir "venv/bin/*" 


PIP="venv/bin/pip"
JUPYTER="venv/bin/jupyter"


if [[ "$1" == "init" || "$1" == $EOF ]]; then
	if [ ! -d "venv" ]; then
		echo "Creando ... carpeta venv"
		echo "Instalando ... entorno virtual"
		virtualenv -p python3 venv
		echo "Instalando ... jupyter"
		$PIP install jupyter
		echo "Iniciando jupyter"
		$JUPYETER notebook
	else
		$JUPYTER notebook
	fi
elif [[ "$1" = "install" ]]; then
	$PIP "install" $2

elif [[ "$1" = "packages" ]]; then
	$PIP "freeze"
fi




