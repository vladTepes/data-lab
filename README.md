# data-lab

Hacklab + datos = dataLab

# 1. Repositorio

Debemos asegurarnos tener *git* instalado y clonamos el repositorio. Siempre 
podrás traerte los cambios que se suban, no obstante, si deseas aportar los tuyos
deberás crearte una cuenta en el gitlab de 0xacab.

## clonando el repositorio

``` 
git clone https://0xacab.org/vladTepes/data-lab.git
```

Si deseas traer los cambios para cada encuentro, sugiero crear una rama y treaer
los de esa sesión


``` 
data-lab $ git checkout -b <nombre_encuentro>
```
git nos va a posicionar en la nueva rama y desde ahí. 


``` 
data-lab $ git pull origin master
```

si quieres viajar en el tiempo a encuentros anteriores.


``` 
data-lab $ git checkout <nombre_encuentro_anterior>
```

De esta forma no sobre escribirás lo que vas realizando. 
# 2. Requisitos e instalación

Hay tantas formas para instalar como herramientas por usar, no obstante, de
preferencia se debe trabajar con entornos virutales. Python generalmente viene de 
forma nativa en nuestros equipos, además, es muy probable que tengan instalado
python2 y python3. Por otro lado, en ocasiones puedes necesitar trabajar
con diferentes versiones de cada librería. Por esta razón, para tener de forma 
ordenada nuestro trabajo, sea más fácil documentar y compartir; es necesario
utulizar por lo menos una de estas formas para trabajar:

- virtualenv
- Anaconda
- miniconda

En mi opinión, la ventaja de virtualenv frente a Anaconda, es precisamente que no vienen
los paquetes instalados por defecto por lo que es ligero y se adecua a cada proyecto.
La desventaja, es que para cada proyecto se debe crear un virtualenv y gestionarlos
es menos sencillo que con anaconda.

Prueba ambos !! :)



## Virtualenv
Para instalar virtualenv, primero necesitamos instalar pip, el gestor de paquetes de python. 

Debian/Ubuntu
``` 
sudo apt install python3-pip
```

luego instalamos virtualenv

``` 
sudo pip install virtualenv
```

### Creando un proyecto

Una vez clonado el repositorio, creamos un entorno virtual dentro, el cual solo 
estara de forma local en nuestras computadoras. De ahora en adelante,
siempre vamos a trabajar iniciando virtualenv.


Una opción es mediante *culebrita.sh*, para ver cómo funciona revisa la pagina de la wiki
de este repositorio [aqui](https://0xacab.org/vladTepes/data-lab/wikis/culebrita)

La otra opción es directamente paso a paso. Se sugiere entender estos pasos y así entender
que el script **culebrita.sh** es lo mismo. 

``` 
data-lab $ virtualenv -p python3 venv
```

En este momento, ya podemos instalar jupyter dentro de nuestro proyecto

``` 
data-lab $ venv/bin/pip install jupyter
```

Para instalar cualquier librería, aparte de la ya instalada *jupyter* se debe hacer lo mismo
que en el paso anterior, por ejemplo: instalamos **pandas**

```
data-lab $  venv/bin/pip install pandas
```

### Iniciando venv y jupyter

Como se mencionaba antes, primero debemos iniciar *venv* y posteriormente  *jupyter*

```
data-lab $  venv/bin/activate
```

Si se dan cuenta, aparecerá el nombre *venv* entre parentesis en nuestra consola
esto significa que ya esta activado. para desactivar, puden usar **deactivate** 
en lugar de ** activate**


Ahora, al escribir el siguiente comando se debe iniciar en el navegador la aplicación
**jupyter**

```
data-lab (venv)$  venv/bin/jupyter notebook
```



## Anaconda/miniconda

(alguien podría rellenar)

---

# Jupyter notebook

Jupyter es una aplicación cliente-servidor elaborada como IDE para analisis de datos.
al iniciar un nuevo *notebook* generará en tu carpeta **data-lab** un archivo de extensión
*.ipynb*. Este archvo nunca lo editaremos directamene, solo por medio de *notebook*

    